package com.tjlee.lcs;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 문제 LCS(Longest Common Subsequence, 최장 공통 부분 수열)문제는 두 수열이 주어졌을 때, 모두의 부분 수열이
 * 되는 수열 중 가장 긴 것을 찾는 문제이다. 예를 들어, ACAYKP와 CAPCAK의 LCS는 ACAK가 된다.
 * 
 * 입력 첫째 줄과 둘째 줄에 두 문자열이 주어진다. 문자열은 알파벳 대문자로만 이루어져 있으며, 최대 1000글자로 이루어져 있다. 
 * ex) ACAYKP, CAPCAK
 * 
 * 출력 첫째 줄에 입력으로 주어진 두 문자열의 LCS의 길이를 출력한다. 
 * ex) 4
 * 
 * 풀이방법
 * 1. 동일한 마지막 원소가 있는지 확인 후 삭제 ( 뒤에서부터 삭제된 원소는 LCS의 원소가 됨 ) 
 * 2. 마지막원소는 따로 저장 후 남은 두개의 변수의 원소들로 LCS 찾기 
 * 		1) 두 수열 X, Y가 같은 기호일 때 LCS(Xn, Ym) = (LCS( Xn-1, Ym-1), xn) 여기서 xn은 1
 * 		2) 두 수열 X, Y가 같은 기호가 아닐 때 X와 Y의 LCS는 LCS(Xn,Ym-1)와 LCS(Xn-1,Ym)중 더 긴 수열이다.
 * 		     -> 이 공식을 사용하여 배열에 값을 넣는 로직을 구현 
 * 		3) (0, y) , (x, 0) 에는 모두 0을 넣고 계산
 * 
 * ps. dynamic programing(동적 계획법)이란? 
 * Memorization : 중복된 계산을 하지 않게 결과를 저장해 놓기 (cache)
 * bottom-up : 밑에서부터 하나씩 순서대로 계산해서 올라오는 방식이며 recursion이 수반되는 overhead가 없다. (이게 동적계획법) 
 */

@Slf4j
public class LcsExam {
	public static void main(String[] mainargs) {
		String[] args = { "ACAYKP", "CAPCAK" };
//		System.out.println("result : " + calcPoint(makeVO(args1.toString())));
		
		log.info("args : " + Arrays.asList(args).toString());
		VO vo = cutFromBehind(args[0], args[1]);
		
		log.info("result : " + (compareTwoStrAndReturnLCS(vo) + vo.getBehindStr().length()));
//		log.info(vo.toString());
	}

	// 1. 뒤에서부터 잘라서 저장하는 메소드
	public static VO cutFromBehind(String args1, String args2) {
		
		StringBuilder sb = new StringBuilder();
		int i1 = args1.length();
		int i2 = args2.length();
		int forLength = i1 >= i2 ? i2 : i1;	// 작은수만큼 iterate
		
		for(int i = 0; i < forLength; i++) {
//			log.debug("args1.charAt("+ (args1.length() - 1 - i) +") : " + args1.charAt(args1.length() - 1 - i));
//			log.debug("args2.charAt("+ (args2.length() - 1 - i) +") : " + args2.charAt(args2.length() - 1 - i));
			if(args1.charAt(args1.length() - 1 - i) == args2.charAt(args2.length() - 1 - i)) sb.append(args1.charAt(args1.length() - 1 - i));
			else break;
		}
		
		sb.reverse();	// 뒤집기 ,, 숫자만 셀꺼면 안해도 됨 확인용
		args1 = args1.substring(0, args1.length() - sb.toString().length());
		args2 = args2.substring(0, args2.length() - sb.toString().length());
		
		// args length에 1을 더하는 이유는 0번째 배열에 모두 0을 넣고 계산하기 위함 
		return new VO(args1, args2, sb.toString(), new Integer[args1.length()+1][args2.length()+1]);
	}

	// 2. 두개의 String을 비교하여 LCS 리턴
	public static int compareTwoStrAndReturnLCS(VO vo) {
		
		int xlength = vo.getValue1().length() +1;
		int ylength = vo.getValue2().length() +1;
		
		for(int i = 0; i < xlength; i++) {
			for(int j = 0; j < ylength; j++) {
				
				// x,y 0번째 배열에 모두 0을 넣기 
				if(i == 0 || j == 0) {
					vo.getLcsArray()[i][j] = 0;
					continue;
				}
				
				// 글자가 같은 경우 
				if(vo.getValue1().charAt(i-1) == vo.getValue2().charAt(j-1)) {
						vo.getLcsArray()[i][j] = vo.getLcsArray()[i-1][j-1] + 1;
						
				// 글자가 다른 경우 
				}else {
					vo.getLcsArray()[i][j] = 
						vo.getLcsArray()[i-1][j] > vo.getLcsArray()[i][j-1] ? vo.getLcsArray()[i-1][j] : vo.getLcsArray()[i][j-1];
				}
				
//				log.info("i : " + i + ", j : " + j + ", vo.getLcsArray()[i][j]:" + vo.getLcsArray()[i][j]);
			}
		}
		
		return vo.getLcsArray()[xlength-1][ylength-1];
	}
}

@Data
@AllArgsConstructor
class VO {
	private String value1;
	private String value2;
	private String behindStr;
	private Integer[][] lcsArray;
}
