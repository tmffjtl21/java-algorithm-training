package com.tjlee.lcs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaAlgorithmTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaAlgorithmTrainingApplication.class, args);
	}
}
